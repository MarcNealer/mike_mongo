import psutil
import os

def tester():
    found = False
    for proc in psutil.process_iter():
            if 'python' in proc.name():
                details = psutil.Process(pid=proc.pid).cmdline()
                if len(details) > 1:
                    if details[0] =='python':
                        if 'rssread' in details[1]:
                            print(details)
                            found = True
    if not found:
         os.system('python rssread.py')
    else:
        print('already running')

if __name__ == '__main__':
    tester()