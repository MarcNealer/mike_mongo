import pymongo
import multiprocessing
import threading
import feedparser
import time
import datetime
import socket
from time import mktime
import syslog

socket.setdefaulttimeout(5)
no_processes = 8
no_threads = 20


def controller():
    syslog.syslog('rssread Started')
    channel_queue = multiprocessing.Queue()
    process_list = []
    print('processing latest')
    getc = multiprocessing.Process(target=get_channel(channel_queue))
    getc.start()
    time.sleep(2)
    for x in range(no_processes):
        process_list.append(multiprocessing.Process(target=rss_process,
                                                    args=(channel_queue,)))
        process_list[-1].start()
    for proc in process_list:
        proc.join()
    syslog.syslog('rssread completed')


def get_channel(channel_queue):
    print('adding latest')
    db = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                             authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.latest
    for rec in db.find({}):
        if len(rec['url'].strip()) > 6:
            channel_queue.put_nowait(rec)


def rss_process(channel_queue):
    thread_list = []
    for x in range(no_threads):
        thread_list.append(threading.Thread(target=rss_thread, args=[channel_queue]))
        thread_list[-1].start()
    for thread in thread_list:
        thread.join()
    return


def rss_thread(channel_queue):
    db1 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.episodes
    db2 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.latest
    db3 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.channels
    while channel_queue.qsize() > 0:
        try:
            ep_list = []
            channel = channel_queue.get(timeout=15)
            print(channel_queue.qsize())

            nf = feedparser.parse(channel['url'])
            description = ''

            if len(nf['items']) > 0:
                rec = nf.entries[0]
                new_ep = {}
                bad_link=False
                if 'links' in rec:
                    for link in rec['links']:
                        if 'type' in link:
                            if 'audio' in link['type'] or 'video' in link['type']:
                                try:
                                    if 'https://' in link['href'].lower().strip() or \
                                            'http://' in link['href'].lower().strip():
                                        new_ep['link'] = link['href'].strip()
                                        if '?' in link['href']:
                                            new_ep['link'] = link['href'].rsplit('?', 1)[0]

                                except Exception as e:
                                    print('bad link: {}'.format(e))

                update=True
                if 'published_parsed' in rec:
                    try:
                        published = datetime.datetime.fromtimestamp(
                            mktime(
                                rec['published_parsed']
                            )
                        )
                        if published.date() != datetime.date.today():
                            update = False
                        new_ep['publishedDate'] = published

                    except Exception as e:
                        rec['published_parsed']
                        print('bad published date: {}'.format(e))
                if 'publishedDate' in channel:
                    if channel['publishedDate'] == datetime.datetime.today():
                        update = False
                if 'link' in new_ep:
                    if  len(new_ep['link']) == 0:
                        new_ep['link'] = 'no link'
                        bad_link=True
                    if 'link' in rec:
                        if new_ep['link'].lower().strip() == rec['link'].lower().strip():
                            update=False
                    if update:
                        if 'summary' in nf.feed:
                            description += nf.feed['summary']
                        if 'subtitle' in nf.feed:
                            description += nf.feed['subtitle']
                        db3.update_one({'_id': channel['channel']},
                                       {'$set': {'ep_count': len(nf.entries), 'description': description}})
                        if 'title' in rec:
                            title = rec['title']
                        else:
                            title = 'No title'
                        new_ep['createdAt'] = datetime.datetime.now()
                        new_ep['duration'] = ''
                        new_ep['name'] =  title
                        new_ep['updatedAt'] = datetime.datetime.now()
                        new_ep['url'] = nf.url
                        new_ep['channel'] = channel['channel']
                        if 'itunes_duration' in rec:
                            if ':' in rec['itunes_duration']:
                                new_ep['duration'] = rec.itunes_duration
                            else:
                                try:
                                    new_ep['duration'] = str(datetime.timedelta(seconds=int(rec['itunes_duration'])))
                                except:
                                    pass

                        description = ''
                        if 'summary' in rec:
                            description += rec['summary']
                        if 'subtitle' in rec:
                            description += rec['subtitle']
                        new_ep['description'] = description
                        ep_list.append(new_ep)
                        if not bad_link:
                            db1.insert_one(new_ep)
                            db2.delete_many({'url': channel['url']})
                            db2.insert_one(new_ep)
        except Exception as e1:
            pass


def clear_duplicates():
    db1 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.episodes
    for rec in db1.find({}):
        if db1.count_documents({'feed': rec['feed'],
                                'link': rec['link'],
                                'publishedDate': rec['publishedDate']}) > 1:
            db1.delete_one({'feed': rec['feed'],
                            'link': rec['link'],
                            'publishedDate': rec['publishedDate']})
