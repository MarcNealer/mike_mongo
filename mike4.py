import pymongo
import multiprocessing
import threading
import feedparser
import time
import datetime
import socket
from time import mktime, sleep
import syslog

socket.setdefaulttimeout(5)
no_processes = 8
no_threads = 50

def controller():
    syslog.syslog('rssread Started')
    db1 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.episodes
    print('deleting episodes')
    db1.delete_many({})
    channel_queue = multiprocessing.Queue()
    process_list = []
    print('processing latest')
    getc = multiprocessing.Process(target=get_channel, args=(channel_queue,))
    getc.start()
    time.sleep(15)
    for x in range(no_processes):
        process_list.append(multiprocessing.Process(target=rss_process,
                                                    args=(channel_queue,)))
        process_list[-1].start()
        sleep(1)
    for proc in process_list:
        proc.join()
    syslog.syslog('rssread completed')


def get_channel(channel_queue):
    print('adding channels')
    db = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                             authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.channels
    for rec in db.find({}):
        if len(rec['url'].strip()) > 6:
            channel_queue.put_nowait(rec)
            print(channel_queue.qsize())



def rss_process(channel_queue):
    thread_list = []
    for x in range(no_threads):
        thread_list.append(threading.Thread(target=rss_thread, args=[channel_queue]))
        thread_list[-1].start()
        sleep(1)
    for thread in thread_list:
        thread.join()
    return


def rss_thread(channel_queue):
    db1 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.episodes
    while channel_queue.qsize() > 0:
        try:
            channel = channel_queue.get(timeout=15)
            print(channel_queue.qsize())

            nf = feedparser.parse(channel['url'])
            if len(nf['items']) > 0:
                for rec in nf['items']:
                    new_ep = {}
                    bad_link=False
                    if 'links' in rec:
                        for link in rec['links']:
                            if 'type' in link:
                                if 'audio' in link['type'] or 'video' in link['type']:
                                    try:
                                        new_ep['link'] = link['href'].strip()
                                        if '?' in link['href']:
                                            new_ep['link'] = link['href'].rsplit('?', 1)[0]
                                    except Exception as e:
                                        print('bad link: {}'.format(e))

                    if 'published_parsed' in rec:
                        try:
                            if isinstance(rec['published_parsed'], datetime.datetime):
                                new_ep['publishedDate'] = rec['published_parsed']
                            else:
                                published = datetime.datetime.fromtimestamp(
                                    mktime(
                                        rec['published_parsed']
                                    )
                                )
                                new_ep['publishedDate'] = published
                        except Exception as e:
                            print(rec['published_parsed'])
                            print('bad published date: {}'.format(e))
                    if 'link' not in new_ep:
                        new_ep['link'] = 'no link'
                        bad_link = True
                    if  len(new_ep['link']) == 0:
                        new_ep['link'] = 'no link'
                    if 'link' in rec:
                        if new_ep['link'].lower().strip() == rec['link'].lower().strip():
                            update=False
                    if 'title' in rec:
                        title = rec['title']
                    else:
                        title = 'No title'
                    new_ep['createdAt'] = datetime.datetime.now()
                    new_ep['duration'] = ''
                    new_ep['name'] =  title
                    new_ep['updatedAt'] = datetime.datetime.now()
                    new_ep['url'] = nf.url
                    new_ep['channel'] = channel['_id']
                    if 'itunes_duration' in rec:
                        if ':' in rec['itunes_duration']:
                            new_ep['duration'] = rec.itunes_duration
                        else:
                            try:
                                new_ep['duration'] = str(datetime.timedelta(seconds=int(rec['itunes_duration'])))
                            except:
                                pass

                    description = ''
                    if 'summary' in rec:
                        description += rec['summary']
                    if 'subtitle' in rec:
                        description += rec['subtitle']
                    new_ep['description'] = description
                    if not bad_link and ('http://' in new_ep['link'] or 'https://' in new_ep['link']):
                        db1.insert_one(new_ep)
        except Exception as e1:
            print(e1)
            pass


def clear_duplicates():
    db1 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.episodes
    for rec in db1.find({}):
        if db1.count_documents({'feed': rec['feed'],
                                'link': rec['link'],
                                'publishedDate': rec['publishedDate']}) > 1:
            db1.delete_one({'feed': rec['feed'],
                            'link': rec['link'],
                            'publishedDate': rec['publishedDate']})
