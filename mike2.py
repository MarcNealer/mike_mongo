import pymongo
import datetime
from time import mktime
import feedparser
import requests
import csv

backfeed=False

def rss_thread(channelid):
    db1 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.episodes
    db2 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.latest
    db3 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.channels
    try:
        ep_list = []
        channel = db3.find_one({'id': channelid})
        nf = feedparser.parse(channel['url'])
        db3.update_one({'_id':channel['_id']}, {'$set': {'ep_count':len(nf.entries)}})
        if backfeed:
            for rec in nf.entries:
                if 'title' in rec:
                    title = rec['title']
                else:
                    title = 'No title'
                new_ep = {
                    'createdAt': datetime.datetime.now(),
                    'duration': '',
                    'name': title,
                    'updatedAt': datetime.datetime.now(),
                    'channel': channel['_id']
                }
                try:
                    ep_list['link'] = rec['links'][0]['href']
                except:
                    ep_list['link'] = rec['links'][1]['href']
                if 'itunes_duration' in rec:
                    new_ep['duration'] = rec.itunes_duration
                if 'published_parsed' in rec:
                    new_ep['publishedDate'] = datetime.datetime.fromtimestamp(mktime(rec['published_parsed']))
                description =''
                if 'summary' in rec:
                    description += rec['summary']
                if 'subtitle' in rec:
                    description += rec['subtitle']
                new_ep['description'] = description
                ep_list.append(new_ep)
                db1.insert_one(new_ep)
            if ep_list:
                db2.insert_one(ep_list[0])
        else:
            if nf.entries:
                rec = nf.entries[0]
                if 'title' in rec:
                    title = rec['title']
                else:
                    title = 'No title'
                new_ep = {
                    'createdAt': datetime.datetime.now(),
                    'duration': '',
                    'name': title,
                    'updatedAt': datetime.datetime.now(),
                    'channel': channel['_id']
                }
                if 'links' in rec:
                    for link in rec['links']:
                        if 'type' in link:
                            if link['type'] == 'audio/mpeg':
                                new_ep['link'] = link['href']
                if 'itunes_duration' in rec:
                    new_ep['duration'] = rec.itunes_duration
                if 'published_parsed' in rec:
                    new_ep['publishedDate'] = datetime.datetime.fromtimestamp(mktime(rec['published_parsed']))
                description =''
                if 'summary' in rec:
                    description += rec['summary']
                if 'subtitle' in rec:
                    description += rec['subtitle']
                new_ep['description'] = description
                print(new_ep)
                ep_list.append(new_ep)
                db1.insert_one(new_ep)
                db2.insert_one(new_ep)
    except Exception as e1:
        pass



def check_rss_feeds():
    db3 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.channels
    with open('zero_feed_check.csv', 'w') as f:
        csv_file=csv.writer(f)
        s = requests.Session()
        channels = db3.find({'ep_count': {'$lt':1}})
        for channel in channels:
            try:
                resp = s.get(channel['url'], timeout=(10,20))
                csv_file.writerow([channel['url'], resp.status_code])
            except:
                csv_file.writerow([channel['url'], 'Connection Error'])
