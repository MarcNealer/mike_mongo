import pymongo
import multiprocessing
import threading
import feedparser
import time
import datetime
import socket
socket.setdefaulttimeout(5)
from time import mktime
import syslog


no_processes = 5
no_threads = 70
backfeed = False

def controller():
    syslog.syslog('rssread Started')
    print('clearing ')
    db2 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.latest
    db2.delete_many({})
    channel_queue = multiprocessing.Queue()
    process_list = []
    print('processing channels')
    getc = multiprocessing.Process(target=get_channel(channel_queue))
    getc.start()
    time.sleep(2)
    for x in range(no_processes):
        process_list.append(multiprocessing.Process(target=rss_process,
                                                    args=[channel_queue]))
        process_list[-1].start()
    for proc in process_list:
        proc.join()
    syslog.syslog('rssread completed')


def get_channel(channel_queue):
    print('adding channels')
    db = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.channels
    for rec in db.find({}):
        if 'no rss' not in rec['url'].lower() and len(rec['url'].strip()) > 6:
            channel_queue.put_nowait(rec)

def rss_process(channel_queue):
    thread_list = []
    for x in range(no_threads):
        thread_list.append(threading.Thread(target=rss_thread, args=[channel_queue]))
        thread_list[-1].start()
    for thread in thread_list:
        thread.join()

def rss_thread(channel_queue):
    db1 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.episodes
    db2 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.latest
    db3 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.channels
    while channel_queue.qsize() > 1:
        try:
            ep_list = []
            print(channel_queue.qsize())
            channel = channel_queue.get_nowait()
            nf = feedparser.parse(channel['url'])
            description = ''
            if 'summary' in nf.feed:
                description += nf.feed['summary']
            if 'subtitle' in nf.feed:
                description += nf.feed['subtitle']
            db3.update_one({'_id':channel['_id']}, {'$set': {'ep_count':len(nf.entries), 'description': description}})

            if backfeed:
                for rec in nf.entries:
                    if 'title' in rec:
                        title = rec['title']
                    else:
                        title = 'No title'
                    new_ep = {
                        'createdAt': datetime.datetime.now(),
                        'duration': '',
                        'name': title,
                        'updatedAt': datetime.datetime.now(),
                        'url' : nf.url,
                        'channel': channel['_id']
                    }
                    if 'links' in rec:
                        for link in rec['links']:
                            if 'type' in link:
                                if 'audio' in link['type']:
                                    try:
                                        new_ep['link'] = link['href']
                                    except:
                                        print('bad link')
                    if 'itunes_duration' in rec:
                        new_ep['duration'] = rec.itunes_duration
                    if 'published_parsed' in rec:
                        try:
                            new_ep['publishedDate'] = datetime.datetime.fromtimestamp(mktime(rec['published_parsed']))
                        except:
                            print('bad published date')
                    description = ''
                    if 'summary' in rec:
                        description += rec['summary']
                    if 'subtitle' in rec:
                        description += rec['subtitle']
                    new_ep['description'] = description
                    ep_list.append(new_ep)
                    db1.insert_one(new_ep)
                if ep_list:
                    db2.insert_one(ep_list[0])
            else:
                if nf.entries:
                    rec = nf.entries[0]
                    if 'title' in rec:
                        title = rec['title']
                    else:
                        title = 'No title'
                    new_ep = {
                        'createdAt': datetime.datetime.now(),
                        'duration': '',
                        'name': title,
                        'updatedAt': datetime.datetime.now(),
                        'url': nf.url,
                        'channel': channel['_id']
                    }
                    if 'links' in rec:
                        for link in rec['links']:
                            if 'type' in link:
                                if 'audio' in link['type']:
                                    try:
                                        new_ep['link'] = link['href']
                                    except:
                                        print('bad link')
                    if 'itunes_duration' in rec:
                        new_ep['duration'] = rec.itunes_duration
                    if 'published_parsed' in rec:
                        try:
                            new_ep['publishedDate'] = datetime.datetime.fromtimestamp(mktime(rec['published_parsed']))
                        except:
                            print('bad published date')
                    description =''
                    if 'summary' in rec:
                        description += rec['summary']
                    if 'subtitle' in rec:
                        description += rec['subtitle']
                    new_ep['description'] = description
                    ep_list.append(new_ep)
                    db1.insert_one(new_ep)
                    db2.insert_one(new_ep)
        except Exception as e1:
            print(e1)
            pass

def clear_duplicates():
    db1 = pymongo.MongoClient('157.245.212.163', username='jifcast', password='control092019',
                              authSource='jifcast', authMechanism='SCRAM-SHA-256').jifcast.episodes
    for rec in db1.find({}):
        if db1.count_documents({'feed':rec['feed'],
                                'link': rec['link'],
                                'publishedDate':rec['publishedDate']}) > 1:
           db1.delete_one({'feed':rec['feed'],
                           'link': rec['link'],
                           'publishedDate':rec['publishedDate']})




